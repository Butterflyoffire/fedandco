<?php
/**
 * Created by fedandco.
 * User: tom79
 * Date: 08/03/19
 * Time: 19:01
 */
namespace App\Security;

use App\Entity\Gallery;
use App\Entity\Invitation;
use App\Entity\MastodonAccount;
use App\Entity\Message;
use App\Entity\Project;
use App\Entity\ProjectRole;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class ActionVoter extends Voter
{

    private $security;
    private $entityManager;

    const VIEW = 'view';
    const EDIT = 'edit';
    const DELETE = 'delete';

    public function __construct(Security $security, EntityManagerInterface $entityManager)
    {
        $this->security = $security;
        $this->entityManager = $entityManager;
    }

    /**   // these strings are just invented: you can use anything
     * Determines if the attribute and subject are supported by this voter.
     *
     * @param string $attribute An attribute
     * @param mixed $subject The subject to secure, e.g. an object the user wants to access or any other PHP type
     *
     * @return bool True if the attribute and subject are supported, false otherwise
     */
    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, [self::VIEW, self::EDIT, self::DELETE])) {
            return false;
        }
        return true;
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     * It is safe to assume that $attribute and $subject already passed the "supports()" method check.
     *
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }
        //Super admin can see all
        /* if ($this->security->isGranted('ROLE_SUPER_ADMIN')) {
             return true;
         }*/
        switch ($attribute) {
            case self::VIEW:
                return $this->canView($subject, $user);
            case self::EDIT:
                return $this->canEdit($subject, $user);
            case self::DELETE:
                return $this->canDelete($subject, $user);
        }

    }


    private function canView($subject, User $user)
    {
        // if they can edit, they can view
        if ($this->canEdit($subject, $user)) {
            return true;
        }
        if( $subject instanceof Project){
            $count = $this->entityManager->getRepository("App:Project")->canView($subject, $user);
            return $count == 1;
        }else if ( $subject instanceof ProjectRole){
            $count = $this->entityManager->getRepository("App:Project")->canView($subject->getProject(), $user);
            return $count == 1;
        }else if ( $subject instanceof Invitation){
            $count = $this->entityManager->getRepository("App:Invitation")->canEdit($subject, $user);
            return $count == 1;
        }else if ( $subject instanceof Gallery){
            $this->entityManager->getRepository("App:Project");
            $count = $this->entityManager->getRepository("App:Invitation")->canEdit($subject, $user);
            return $count == 1;
        }else if ( $subject instanceof User){
            return true;
        }else if ( $subject instanceof Message){
            $count = $this->entityManager->getRepository("App:Message")->canEdit($subject, $user);
            return $count == 1;
        }
    }

    private function canEdit($subject, User $user)
    {
        if( $subject instanceof Project){
            $count = $this->entityManager->getRepository("App:Project")->canEdit($subject, $user);
            return $count > 0;
        }else if ( $subject instanceof ProjectRole){
            $count = $this->entityManager->getRepository("App:Project")->canEdit($subject->getProject(), $user);
            return $count == 1;
        }else if ( $subject instanceof Invitation){
            $count = $this->entityManager->getRepository("App:Invitation")->canEdit($subject, $user);
            return $count == 1;
        }else if ( $subject instanceof Gallery){
            $count = $this->entityManager->getRepository("App:Invitation")->canEdit($subject, $user);
            return $subject->getOwner() === $user;
        }else if ( $subject instanceof User){
            return ($subject === $user);
        }else if ( $subject instanceof MastodonAccount){
            return ($subject->getOwner() === $user);
        }else if ( $subject instanceof Message){
            $count = $this->entityManager->getRepository("App:Message")->canEdit($subject, $user);
            return $count == 1;
        }
    }

    private function canDelete($subject, User $user)
    {
        if ($subject instanceof Project) {
            return ($subject->getOwner() === $user);

        }
    }

}
