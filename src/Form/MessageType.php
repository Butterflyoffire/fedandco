<?php
/**
 * Created by fedandco.
 * User: tom79
 * Date: 13/03/19
 * Time: 14:57
 */

namespace App\Form;

use App\Entity\Message;
use Sonata\AdminBundle\Form\Type\CollectionType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Sonata\MediaBundle\Form\Type\MediaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Doctrine\ORM\Query\Expr;
use Symfony\Component\Security\Core\Security;

class MessageType extends AbstractType {


    private $securityContext;

    public function __construct(Security $securityContext)
    {
        $this->securityContext = $securityContext;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('accounts', EntityType::class, [
            'class' => 'App\Entity\MastodonAccount',
            'query_builder' => function ($er) {

                $queryNotIn = $er->createQueryBuilder('ma')
                    ->leftjoin('App:Project', 'p', Expr\Join::WITH, 'ma.id = p.social_account')
                    ->addSelect('ma')
                    ->andWhere(
                        'p.owner = :user '
                    )
                    ->setParameter('user', $this->securityContext->getToken()->getUser())
                    ->getQuery()
                    ->getResult();
                if (empty($queryNotIn)) {
                    $query = $er->createQueryBuilder('ma')
                        ->andWhere(
                            'ma.account_owner = :user'
                        )
                        ->setParameter('user', $this->securityContext->getToken()->getUser());
                } else {
                    $query = $er->createQueryBuilder('ma')
                        ->andWhere(
                            'ma.account_owner = :user AND ma.id NOT IN (:values)'
                        )
                        ->setParameter('values', $queryNotIn)
                        ->setParameter('user', $this->getUser());
                }
                return $query;
            }
        ],['admin_code' => 'admin.composer']);
        $builder->add('content_warning');
        $builder->add('content');
        $builder->add('visibility', ChoiceType::class,
            [
                'choices' => [
                    'status.visibility.public' => 'public',
                    'status.visibility.unlisted' => 'unlisted',
                    'status.visibility.private' => 'private',
                    'status.visibility.direct' => 'direct',
                ]
            ]);
        $builder->add('attachments', CollectionType::class,
            [
                "allow_add" => true,
                "allow_delete" => true,
                'entry_type' => MediaType::class,
                "entry_options" => ['context' =>'default', 'provider' => 'sonata.media.provider.file'],
            ]);
        $builder->add('scheduled');
        $builder->add('scheduled_at', \Symfony\Component\Form\Extension\Core\Type\DateTimeType::class,[
            'widget' => 'single_text',
        ]);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Message::class,
            'translation_domain' => 'fedandco'
        ]);
    }

}