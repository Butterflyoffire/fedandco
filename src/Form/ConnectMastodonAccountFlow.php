<?php
/**
 * Created by fedandco.
 * User: tom79
 * Date: 13/03/19
 * Time: 14:58
 */

namespace App\Form;

use App\Entity\Client;
use Craue\FormFlowBundle\Form\FormFlow;
use Craue\FormFlowBundle\Form\FormFlowInterface;

class ConnectMastodonAccountFlow extends FormFlow {

    protected function loadStepsConfig() {
        return [
            [
                'form_type' => ConnectMastodonAccountType::class,
            ],
            [
                'form_type' => ConnectMastodonAccountType::class,
            ],
            [
                'label' => 'confirmation',
            ],
        ];
    }
}