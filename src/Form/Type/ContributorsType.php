<?php
/**
 * Created by fedandco.
 * User: tom79
 * Date: 09/03/19
 * Time: 11:02
 */

// Not used but kept for example

namespace App\Form\Type;


use App\Entity\Invitation;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContributorsType extends AbstractType
{



    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Invitation::class,
        ]);
    }

    public function getParent()
    {
        return EntityType::class;
    }
}