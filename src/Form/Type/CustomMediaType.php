<?php
/**
 * Created by fedandco.
 * User: tom79
 * Date: 11/04/19
 * Time: 09:44
 */

namespace App\Form\Type;


use Sonata\MediaBundle\Form\Type\MediaType;
use Sonata\MediaBundle\Form\DataTransformer\ProviderDataTransformer;
use Symfony\Component\Form\FormBuilderInterface;

class CustomMediaType extends MediaType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $dataTransformer = new ProviderDataTransformer($this->pool, $this->class, [
            'provider' => $options['provider'],
            'context' => $options['context'],
            'empty_on_new' => $options['empty_on_new'],
            'new_on_update' => $options['new_on_update'],
        ]);
        $dataTransformer->setLogger($this->logger);

        $builder->addModelTransformer($dataTransformer);


        $this->pool->getProvider($options['provider'])->buildMediaType($builder);

        $builder->add('description');
    }


}