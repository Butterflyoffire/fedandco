<?php
/**
 * Created by fedandco.
 * User: tom79
 * Date: 09/03/19
 * Time: 11:02
 */

// Not used but kept for example

namespace App\Form\Type;


use App\Entity\Invitation;
use App\Entity\Media;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomMediaDescriptionType extends AbstractType
{



    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder->add('binaryContent', FileType::class, ['required' => false]);
        $builder->add('description', null, ['required' => false]);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Media::class,
        ]);
    }


}