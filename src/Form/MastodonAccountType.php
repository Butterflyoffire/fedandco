<?php
/**
 * Created by fedandco.
 * User: tom79
 * Date: 13/03/19
 * Time: 14:57
 */

namespace App\Form;
use App\Entity\CustomField;
use App\Entity\MastodonAccount;
use Sonata\AdminBundle\Form\Type\CollectionType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MastodonAccountType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('avatar', FileType::class,['data_class' => null, 'required' => false]);
        $builder->add('header', FileType::class,['data_class' => null, 'required' => false]);
        $builder->add('display_name', null, ['attr' => ['class' => 'col-md-6']]);
        $builder->add('note',null, ['attr' => ['class' => 'col-xs-6']]);
        $builder->add('Fields',  CollectionType::class,  [
            'entry_type' => CustomFieldType::class,
            'allow_add' => true,
            'allow_delete' => true,
            'prototype' => true,
            'entry_options'  => ['required'  => false],]);
        $builder->add('locked');
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MastodonAccount::class,
        ]);
    }

}