<?php
/**
 * Created by fedandco.
 * User: tom79
 * Date: 05/03/19
 * Time: 13:38
 */

namespace App\Admin;



use App\Entity\Project;
use App\Entity\ProjectRole;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ChoiceFieldMaskType;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Doctrine\ORM\Query\Expr;
class ProjectRoleAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {

        if( $this->isCurrentRoute('edit') &&  !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('edit', $this->getSubject()))
            throw new AccessDeniedHttpException();
        $formMapper->add('user', EntityType::class,
            [
                'class' => User::class,
                'choice_label' => 'username',
                'query_builder' => function (EntityRepository $er) {
                    $query =  $er->createQueryBuilder('u')
                        ->leftJoin('App:Invitation', 'i1', Expr\Join::WITH, 'u.id = i1.toUser')
                        ->leftJoin('App:Invitation', 'i2', Expr\Join::WITH, 'u.id = i2.fromUser')
                        ->andWhere(
                            '(i1.fromUser = :user OR i1.toUser = :user OR i2.fromUser = :user OR i2.toUser = :user ) AND (i1.accepted = 1 OR i2.accepted = 1)'
                        )
                        ->setParameter('user',  $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser());

                    return $query;
                },
            ]
        );
        $formMapper->add('project', EntityType::class,
            [
                'class' => Project::class,
                'choice_label' => 'name',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p')
                        ->leftJoin('App:ProjectRole', 'pr', Expr\Join::WITH, 'p.id = pr.project')
                        ->where(
                            '(p.owner = :user OR (pr.User = :user AND pr.role = \'ADMIN\'))'
                        )
                        ->setParameter('user', $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser())
                        ->orderBy('p.name', 'ASC');
                },
            ]
        );

        $translator = $this->getTranslator();
        $formMapper->add('role', ChoiceFieldMaskType::class,
            ['choices' =>
                [
                    $translator->trans('project.role.user',[], 'fedandco', 'en') => 'USER',
                    $translator->trans('project.role.author',[], 'fedandco', 'en') => 'AUTHOR',
                    $translator->trans('project.role.admin',[], 'fedandco', 'en') => 'ADMIN',
                ]
            ]
        );
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {

    }

    public function prePersist($project)
    {

        if( $this->isCurrentRoute('edit') &&  !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('edit', $this->getSubject()))
            throw new AccessDeniedHttpException();

    }

    public function preDelete()
    {
        if( !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('edit', $this->getSubject()))
            throw new AccessDeniedHttpException();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper): void
    {
        if( !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('edit', $this->getSubject()))
            throw new AccessDeniedHttpException();

    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $translator = $this->getTranslator();
        $listMapper->add('user');
        $listMapper->addIdentifier('project');
        $listMapper->add('role',"choice",
            [
                'editable'=> true,
                'choices' =>
                    [
                        'USER' => $translator->trans('project.role.user',[], 'fedandco', 'en'),
                        'AUTHOR' => $translator->trans('project.role.author',[], 'fedandco', 'en'),
                        'ADMIN' => $translator->trans('project.role.admin',[], 'fedandco', 'en'),
                    ]

            ]);
    }


    public function toString($object)
    {
        return $object instanceof ProjectRole
            ? $object->getUser()->getEmail()
            : 'User ';
    }
}