<?php
/**
 * Created by fedandco.
 * User: tom79
 * Date: 05/03/19
 * Time: 13:38
 */

namespace App\Admin;




use App\Entity\Project;
use App\Entity\User;
use App\Form\ProjectRoleType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\CollectionType;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\Form\Validator\ErrorElement;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Doctrine\ORM\Query\Expr;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;


class ProjectAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {

        if($this->isCurrentRoute('edit') && !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('edit', $this->getSubject()))
            throw new AccessDeniedHttpException();
        $translator = $this->getTranslator();


        if ($this->isCurrentRoute('create')) {
            $formMapper->with( $translator->trans('common.project',[], 'fedandco', 'en'));
            $formMapper->add('name', TextType::class);
            $formMapper->remove('createdAt');
            $formMapper->remove('owner');
            $formMapper->end();
            $formMapper->remove('social_account');
        }else{

            $formMapper
                ->tab($translator->trans('common.project',[], 'fedandco', 'en'))
                ->with($translator->trans('common.project',[], 'fedandco', 'en'), ['class' => 'col-md-6'])->end()
                ->with($translator->trans('project.contributors',[], 'fedandco', 'en'), ['class' => 'col-md-6'])->end()
                ->end()
            ;

            $options['project'] = array($this->getSubject());
            $project = $this->getSubject();
            $current_account = -1; //No id will match this value
            if( $project->getSocialAccount())
                $current_account = $project->getSocialAccount()->getId();
            $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
            $formMapper->remove('createdAt');
            $formMapper->remove('owner');

            $formMapper->tab($translator->trans('common.project',[], 'fedandco', 'en'));
            $formMapper->with( $translator->trans('common.project',[], 'fedandco', 'en'));
            $formMapper->add('name', TextType::class);
            $formMapper->end();
            if( $project->getOwner() == $user) {
                $formMapper->with('Social account');
                $formMapper->add('social_account', EntityType::class,
                    [
                        'required' => false,
                        'class' => 'App\Entity\MastodonAccount',
                        'placeholder' => $translator->trans('mastodon.selection.no_account', [], 'fedandco', 'en'),
                        'query_builder' => function ($er) use ($current_account) {


                            $queryNotIn = $er->createQueryBuilder('ma')
                                ->leftjoin('App:Project', 'p', Expr\Join::WITH, 'ma.id = p.social_account')
                                ->addSelect('ma')
                                ->andWhere(
                                    'p.owner = :user AND p.social_account != :current_account'
                                )
                                ->setParameter('current_account', $current_account)
                                ->setParameter('user', $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser())
                                ->getQuery()
                                ->getResult();
                            if (empty($queryNotIn)) {
                                $query = $er->createQueryBuilder('ma')
                                    ->andWhere(
                                        'ma.account_owner = :user'
                                    )
                                    ->setParameter('user', $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser());
                            } else {
                                $query = $er->createQueryBuilder('ma')
                                    ->andWhere(
                                        'ma.account_owner = :user AND ma.id NOT IN (:values)'
                                    )
                                    ->setParameter('values', $queryNotIn)
                                    ->setParameter('user', $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser());
                            }
                            return $query;
                        }

                    ], ['admin_code' => 'admin.project']);
                $formMapper->end();
            }else
                $formMapper->remove('social_account');


            $formMapper->end();
            $formMapper->tab($translator->trans('project.contributors',[], 'fedandco', 'en'));
            $formMapper->with($translator->trans('project.contributors',[], 'fedandco', 'en'));
            $formMapper->add('contributor_role', CollectionType::class, [
                'entry_options' => ['project' => $this->getSubject()],
                "allow_add" => true,
                "allow_delete" => true,
                'entry_type' => ProjectRoleType::class

            ]);
            $formMapper->end();
            $formMapper->end();

        }
    }


    /*public function getFormTheme() {
        return array_merge(
            parent::getFormTheme(), array(
                'Sonata/ProjectAdmin/customers_edit.html.twig')
        );
    }*/

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
        //Filter for known contributors only
        $datagridMapper->add('contributor_role', null, [], null,
            [
                'class' => User::class,
                'label' => $this->trans("project.contributors",[],'fedandco','en'),
                'query_builder' => function($er) {
                    $query =  $er->createQueryBuilder('u')
                        ->leftJoin('App:Invitation', 'i1', Expr\Join::WITH, 'u.id = i1.toUser')
                        ->leftJoin('App:Invitation', 'i2', Expr\Join::WITH, 'u.id = i2.fromUser')
                        ->andWhere(
                            '(i1.fromUser = :user OR i1.toUser = :user OR i2.fromUser = :user OR i2.toUser = :user ) AND (i1.accepted = 1 OR i2.accepted = 1)'
                        )
                        ->setParameter('user',  $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser());

                    return $query;
                }
        ]
        );
    }


    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return 'Sonata/Extends/edit.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }


    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper): void
    {
        if( $this->isCurrentRoute('read') &&  !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('edit', $this->getSubject()))
            throw new AccessDeniedHttpException();

    }

    public function preRemove($project){
        if (false === $this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('delete', $project)) {
            throw new AccessDeniedHttpException();
        }
    }

    /**
     * {@inheritdoc}
     * https://github.com/sonata-project/SonataAdminBundle/pull/1318
     */
    public function preBatchAction($actionName, ProxyQueryInterface $query, array & $idx, $allElements)
    {
        if (true === $allElements) {
            throw new \InvalidArgumentException('Not supported');
        }

        if ('delete' === $actionName) {
            foreach ($idx as $id) {
                $file = $this->getObject($id);
                $this->preRemove($file);
            }
        }
    }

    /** @var $project Project */
    public function prePersist($project)
    {

        if( $this->isCurrentRoute('edit') &&  !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('edit', $this->getSubject()))
            throw new AccessDeniedHttpException();
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        try {
            $project->setCreatedAt(new \DateTime());
            $project->setOwner($user);
        } catch (\Exception $e) {
        }
    }


    // add this method
    public function validate(ErrorElement $errorElement, $project)
    {
        $contributorAdded = [];
        if( $project instanceof Project ){
            foreach ($project->getContributorRole() as $contributor){
                if( !in_array($contributor->getUser()->getId(),$contributorAdded ))
                    $contributorAdded[] = $contributor->getUser()->getId();
               else{
                   $errorElement
                       ->with('contributor_role')
                       ->addViolation($this->trans("error.contributor.already_added",[],"fedandco","en"))
                       ->end()
                   ;
               }
            }
        }

    }


    /**
     * {@inheritdoc}
     */
    public function configureBatchActions($actions)
    {
        if (isset($actions['delete'])) {
            unset($actions['delete']);
        }

        return $actions;
    }

    public function preDelete()
    {
        if( !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('edit', $this->getSubject()))
            throw new AccessDeniedHttpException();
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name');
        $listMapper->add('createdAt');
        $listMapper->add('contributors');
    }


    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('export');
    }


    public function createQuery($context = 'list')
    {
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $query = parent::createQuery($context);
        $query->leftJoin('App:ProjectRole', 'pr', Expr\Join::WITH, $query->getRootAliases()[0] .'.id = pr.project');
        $query->andWhere(
            $query->getRootAliases()[0] . '.owner = :user OR (pr.User = :user AND pr.role = \'ADMIN\')'
        );
        $query->setParameter('user', $user);
        return $query;
    }

    public function toString($object)
    {
        return $object instanceof Project
            ? $object->getName()
            : 'Project ';
    }

}