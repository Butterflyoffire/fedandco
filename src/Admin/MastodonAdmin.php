<?php
/**
 * Created by fedandco.
 * User: tom79
 * Date: 05/03/19
 * Time: 13:38
 */

namespace App\Admin;



use App\Entity\CustomField;
use App\Entity\MastodonAccount;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;


class MastodonAdmin extends AbstractAdmin
{

    protected $baseRouteName = 'admin_mastodon';
    protected $baseRoutePattern = 'mastodon';


    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {


    }
    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper): void
    {
        if($this->isCurrentRoute('edit') && !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('edit', $this->getSubject()))
            throw new AccessDeniedHttpException();
    }


    public function prePersist($invitation)
    {

    }

    public function preDelete()
    {

    }

    public function createQuery($context = 'list')
    {
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $query = parent::createQuery($context);
        $query->andWhere(
            $query->getRootAliases()[0] . '.account_owner = :user '
        );
        $query->setParameter('user', $user);
        return $query;
    }


    protected function configureListFields(ListMapper $listMapper)
    {


        $listMapper->addIdentifier('avatar', null, ['template' => 'Client/Accounts/profile_image_field.html.twig']);
        $listMapper->add('username');
        $listMapper->addIdentifier('acct');
        $listMapper->add('statuses_count');
        $listMapper->add('followers_count');
        $listMapper->add('following_count');
        $listMapper->add('note', null, ['template' => 'Client/Accounts/note_field.html.twig']);
    }


    protected function configureRoutes(RouteCollection $collection)
    {
    }



    public function toString($object)
    {
        return $object instanceof MastodonAccount
            ? $object->getAcct() . "@" . $object->getInstance()
            : 'MastodonAccount ';
    }



}