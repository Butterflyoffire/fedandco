<?php
/**
 * Created by fedandco.
 * User: tom79
 * Date: 05/03/19
 * Time: 13:38
 */

namespace App\Admin;




use App\Entity\MastodonAccount;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Doctrine\ORM\Query\Expr;

class NotificationAdmin extends AbstractAdmin
{

    protected $baseRouteName = 'admin_notification';
    protected $baseRoutePattern = 'notification';



    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {


    }

    protected function configureRoutes(RouteCollection $collection)
    {
        parent::configureRoutes($collection);
        $collection->remove('create');
        $collection->remove('delete');
        $collection->remove('edit');
        $collection->add('load_more', $this->getRouterIdParameter().'/load-more',
            [],
            [],
            ['expose' => true],
            '',
            [],
            ['POST']
        );
}


    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper): void
    {
        if( !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('edit', $this->getSubject()))
            throw new AccessDeniedHttpException();

    }


    public function prePersist($invitation)
    {
        if( $this->isCurrentRoute('edit') &&  !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('edit', $this->getSubject()))
            throw new AccessDeniedHttpException();

    }

    public function preDelete()
    {
        if( !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('edit', $this->getSubject()))
            throw new AccessDeniedHttpException();

    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $translator = $this->getTranslator();
        $listMapper->addIdentifier('avatar', null, [
            'label' => $translator->trans('common.accounts',[], 'fedandco', 'en'),
            'template' => 'Mastodon/Notifications/notif_list_field.html.twig'
        ]);
    }

    /**
     * Custom createQuery to display Social accounts that belong to the connected accounts or having right to see it.
     * @param string $context
     * @return \Sonata\AdminBundle\Datagrid\ProxyQueryInterface
     */
    public function createQuery($context = 'list')
    {
        $queryIn = parent::createQuery($context);
        $queryIn->add('select', 'IDENTITY(p.social_account)', false );
        $queryIn->add('from', 'App\Entity\Project p', false );
        $queryIn->leftJoin("p.social_account", "a", Expr\Join::WITH, 'p.social_account = a.id')
            ->leftJoin("p.contributor_role", "pr")
            ->where(
                'p.social_account IS NOT NULL AND (p.owner = :user OR pr.User = :user OR a.account_owner = :user)'
            )
            ->setParameter('user', $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser());

        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $query = parent::createQuery($context);
        $query->andWhere(
            $query->getRootAliases()[0] . '.account_owner = :user OR '.$query->getRootAliases()[0] .'.id IN (:accounts)'
        );
        $query->setParameter('user', $user);
        $query->setParameter('accounts', $queryIn->getQuery()->getResult());

        return $query;
    }



    public function toString($object)
    {
        return $object instanceof MastodonAccount
            ? $object->getAcct()."@".$object->getInstance()
            : 'Account ';
    }



}