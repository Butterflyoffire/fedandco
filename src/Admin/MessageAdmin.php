<?php
/**
 * Created by fedandco.
 * User: tom79
 * Date: 05/03/19
 * Time: 13:38
 */

namespace App\Admin;




use App\Command\SendMessageCommand;
use App\Entity\Message;
use App\Form\Type\CustomMediaDescriptionType;
use App\Form\Type\CustomMediaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Doctrine\ORM\Query\Expr;

class MessageAdmin extends AbstractAdmin
{

    protected $baseRouteName = 'admin_message';
    protected $baseRoutePattern = 'message';


    protected function configureFormFields(FormMapper $formMapper)
    {

        if ($this->isCurrentRoute('edit') && !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('edit', $this->getSubject()))
            throw new AccessDeniedHttpException();


        $translator = $this->getTranslator();
        $formMapper->add('social_account', EntityType::class, [
            'class' => 'App\Entity\MastodonAccount',
            'query_builder' => function ($er) {

                $queryNotIn = $er->createQueryBuilder('ma')
                    ->leftjoin('App:Project', 'p', Expr\Join::WITH, 'ma.id = p.social_account')
                    ->addSelect('ma')
                    ->andWhere(
                        'p.owner = :user '
                    )
                    ->setParameter('user', $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser())
                    ->getQuery()
                    ->getResult();
                if (empty($queryNotIn)) {
                    $query = $er->createQueryBuilder('ma')
                        ->andWhere(
                            'ma.account_owner = :user'
                        )
                        ->setParameter('user', $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser());
                } else {
                    $query = $er->createQueryBuilder('ma')
                        ->andWhere(
                            'ma.account_owner = :user AND ma.id NOT IN (:values)'
                        )
                        ->setParameter('values', $queryNotIn)
                        ->setParameter('user', $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser());
                }
                return $query;
            }
        ],['admin_code' => 'admin.composer']);
        $formMapper->add('content_warning');
        $formMapper->add('content');

        $formMapper->add('visibility', ChoiceType::class,
            [
                'choices' => [
                    $translator->trans('status.visibility.public',[], 'fedandco', 'en')=> 'public',
                    $translator->trans('status.visibility.unlisted',[], 'fedandco', 'en')=> 'unlisted',
                    $translator->trans('status.visibility.private',[], 'fedandco', 'en')=> 'private',
                    $translator->trans('status.visibility.direct',[], 'fedandco', 'en')=> 'direct',

                    ]
            ]);
        $formMapper->add('attachments', CollectionType::class,
            [
                "allow_add" => true,
                "allow_delete" => true,
                'entry_type' => CustomMediaDescriptionType::class
            ]);
        $formMapper->add('scheduled');
        $formMapper->add('scheduled_at', DateTimeType::class,[
            'widget' => 'single_text',
            'required' => false
        ]);

    }

    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return 'Mastodon/Message/edit.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {


    }

    protected function configureRoutes(RouteCollection $collection)
    {
        parent::configureRoutes($collection);
    }


    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper): void
    {
        if( !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('edit', $this->getSubject()))
            throw new AccessDeniedHttpException();

    }


    public function preUpdate($message)
    {
        if( $this->isCurrentRoute('edit') &&  !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('edit', $this->getSubject()))
            throw new AccessDeniedHttpException();
        /** @var $message Message */
        try {
            if( $message->getAttachments() !== null && count($message->getAttachments()) > 0){
                foreach ($message->getAttachments() as $attachment){
                    if( $attachment->getBinaryContent() == null ){
                        $message->removeAttachment($attachment);
                    }else{
                        $attachment->setContext("default");
                        $attachment->setProviderName('sonata.media.provider.file');
                    }
                }
            }

        } catch (\Exception $e) {
        }

    }

    public function prePersist($message)
    {
        if( $this->isCurrentRoute('edit') &&  !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('edit', $this->getSubject()))
            throw new AccessDeniedHttpException();
        /** @var $message Message */
        try {
            $message->setCreatedAt(new \DateTime());
            if( $message->getAttachments() !== null && count($message->getAttachments()) > 0){
                foreach ($message->getAttachments() as $attachment){
                    if( $attachment->getBinaryContent() == null ){
                        $message->removeAttachment($attachment);
                    }else{
                        $attachment->setContext("default");
                        $attachment->setProviderName('sonata.media.provider.file');
                    }
                }
            }
        } catch (\Exception $e) {
        }
    }

    public function postPersist($message)
    {
        /** @var $message Message */
        if( !$message->getScheduled()) {
            $em = $this->getConfigurationPool()->getContainer()->get('doctrine.orm.entity_manager');
            $ma = $this->getConfigurationPool()->getContainer()->get('mastodon.api');
            $fp = $this->getConfigurationPool()->getContainer()->get('sonata.media.provider.file');
            $sendMessage = new SendMessageCommand($em, $ma, $fp);
            $input = new ArrayInput([
                'message_id' => $message->getId(),
            ]);
            $output = new NullOutput();
            try {
                $sendMessage->run($input, $output);
            } catch (\Exception $e) {
                echo $e->getMessage();
                echo $e->getTraceAsString();exit;
            }
        }
    }


    public function preDelete()
    {
        if( !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('edit', $this->getSubject()))
            throw new AccessDeniedHttpException();

    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('social_account',null,['admin_code' => 'admin.composer']);
        $listMapper->add('content');
        $listMapper->add('created_at');
        $listMapper->add('visibility');
        $listMapper->add('scheduled');
        $listMapper->add('sent', 'boolean');
        $listMapper->add('totalMedia');
    }

    /**
     * Custom createQuery to display Social accounts that belong to the connected accounts or having right to see it.
     * @param string $context
     * @return \Sonata\AdminBundle\Datagrid\ProxyQueryInterface
     */
    public function createQuery($context = 'list')
    {
       /* $queryIn = parent::createQuery($context);
        $queryIn->add('select', 'IDENTITY(p.social_account)', false );
        $queryIn->add('from', 'App\Entity\Project p', false );
        $queryIn->leftJoin("p.social_account", "a", Expr\Join::WITH, 'p.social_account = a.id')
            ->leftJoin("p.contributor_role", "pr")
            ->where(
                'p.social_account IS NOT NULL AND (p.owner = :user OR pr.User = :user OR a.account_owner = :user)'
            )
            ->setParameter('user', $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser());

        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $query = parent::createQuery($context);
        $query->add('select', 'a', false );
        $query->add('from', 'App\Entity\MastodonAccount a', false );
        $query->andWhere(
            'a.account_owner = :user OR a.id IN (:accounts)'
        );
        $query->setParameter('user', $user);
        $query->setParameter('accounts', $queryIn->getQuery()->getResult());
        */
        $query = parent::createQuery($context);
        return $query;
    }



    public function toString($object)
    {
        return $object instanceof Message
            ? $object->getContent()
            : 'Message ';
    }



}