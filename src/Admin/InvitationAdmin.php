<?php
/**
 * Created by fedandco.
 * User: tom79
 * Date: 05/03/19
 * Time: 13:38
 */

namespace App\Admin;



use App\Entity\Invitation;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;


class InvitationAdmin extends AbstractAdmin
{

    protected $baseRouteName = 'admin_invitation';
    protected $baseRoutePattern = 'invitation';

    protected function configureFormFields(FormMapper $formMapper)
    {
        if( $this->isCurrentRoute('edit') &&  !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('edit', $this->getSubject()))
            throw new AccessDeniedHttpException();

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {


    }
    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper): void
    {
        if( !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('edit', $this->getSubject()))
            throw new AccessDeniedHttpException();

    }


    public function prePersist($invitation)
    {
        if( $this->isCurrentRoute('edit') &&  !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('edit', $this->getSubject()))
            throw new AccessDeniedHttpException();

    }

    public function preDelete()
    {
        if( !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('edit', $this->getSubject()))
            throw new AccessDeniedHttpException();

    }


    protected function configureListFields(ListMapper $listMapper)
    {

        $translator = $this->getTranslator();
        $listMapper->add('fromUser', null,
            [
                'label' => $translator->trans('project.role.user',[], 'fedandco', 'en'),
                'route' => ['name' => 'show']
            ]
        );
        $listMapper->add('accepted', null, ['label' => $translator->trans('invitation.accept',[], 'fedandco', 'en'), 'editable' => true]);
    }

    public function createQuery($context = 'list')
    {
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $query = parent::createQuery($context);
        /*$query->andWhere(
            $query->expr()->eq($query->getRootAliases()[0] . '.accepted', ':accepted')
        );
        $query->setParameter('accepted', true);*/
        $query->andWhere(
            $query->expr()->eq($query->getRootAliases()[0] . '.toUser', ':toUser')
        );
        $query->setParameter('toUser', $user);

        return $query;
    }
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('edit');
        $collection->remove('export');
    }



    public function toString($object)
    {
        return $object instanceof Invitation
            ? $object->getEmail()
            : 'User ';
    }



}