<?php
/**
 * Created by fedandco.
 * User: tom79
 * Date: 05/03/19
 * Time: 13:38
 */

namespace App\Admin;



use App\Entity\Invitation;
use App\Entity\User;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\Form\Validator\ErrorElement;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;


class ContactAdmin extends AbstractAdmin
{

    protected $baseRouteName = 'admin_contact';
    protected $baseRoutePattern = 'contact';

    protected function configureFormFields(FormMapper $formMapper)
    {
        if( $this->isCurrentRoute('edit') &&  !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('edit', $this->getSubject()))
            throw new AccessDeniedHttpException();
        $translator = $this->getTranslator();
        $formMapper->add('email', EmailType::class, ['label' => $translator->trans('common.email',[], 'fedandco', 'en') ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {


    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper): void
    {
        if( !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('edit', $this->getSubject()))
            throw new AccessDeniedHttpException();
    }



    /**
     * Manage invitations before submitting them
     * @param $invitation
     * @throws \Exception
     */
    public function prePersist($invitation)
    {
        if( $this->isCurrentRoute('edit') &&  !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('edit', $this->getSubject()))
            throw new AccessDeniedHttpException();
        $container = $this->getConfigurationPool()->getContainer();
        $user = $container->get('security.token_storage')->getToken()->getUser();
        $doctrine = $container->get('doctrine');
        //Check if invitation already exists
        $invitationExist = $doctrine->getRepository('App:Invitation')->findOneBy(['fromUser' => $user,'email' => $invitation->getEmail()]);
        //The invitation doesn't exist
        if(! $invitationExist) {
            $invitation->setFromUser($user);
            $toUSer = $doctrine->getRepository('App:User')->findOneBy(['email' => $invitation->getEmail()]);
            if (!$toUSer) {
                $toUSer = new User();
                $toUSer->setUsername(explode("@", $invitation->getEmail())[0]);
                $toUSer->setPassword($this->generateRandomString(32));
                $toUSer->setEmail($invitation->getEmail());
                $doctrine->getManager()->persist($toUSer);
            }
            $invitation->setToUser($toUSer);
            $invitation->setFromUser($user);
            $invitation->setCreatedAt(new \DateTime('now'));
            $invitation->setAccepted(false);
        }
    }


    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('export');
    }

    public function validate(ErrorElement $errorElement, $invitation)
    {
        $container = $this->getConfigurationPool()->getContainer();
        $user = $container->get('security.token_storage')->getToken()->getUser();
        $doctrine = $container->get('doctrine');
        //Check if invitation already exists
        $email = trim( $invitation->getEmail());
        $userFromEmail = $doctrine->getRepository('App:User')->findOneBy(['email' => $email]);
        if( $email == trim($user->getEmail())){
            $translator = $this->getTranslator();
            $errorElement
                ->with('email')
                ->addViolation($translator->trans('error.invitation.email_same',[], 'fedandco', 'en'))
                ->end()
            ;
        }
        if( $userFromEmail){
            $invitationExist = $doctrine->getRepository('App:Invitation')->findOneBy(['fromUser' => $user,'toUser' => $userFromEmail]);
            //The invitation doesn't exist
            $translator = $this->getTranslator();
            if($invitationExist) {
                $errorElement
                    ->with('email')
                    ->addViolation($translator->trans('error.invitation.email',[], 'fedandco', 'en'))
                    ->end()
                ;
            }else{
                $invitationExist = $doctrine->getRepository('App:Invitation')->findOneBy(['fromUser' => $userFromEmail,'toUser' => $user]);
                //The invitation doesn't exist
                $translator = $this->getTranslator();
                if($invitationExist) {
                    $errorElement
                        ->with('email')
                        ->addViolation($translator->trans('error.invitation.email',[], 'fedandco', 'en'))
                        ->end()
                    ;
                }
            }
        }
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $translator = $this->getTranslator();
        $listMapper->add('toUser', null,
            [
                'label' => $translator->trans('project.role.user',[], 'fedandco', 'en'),
                'route'=> ['name'=>'show'],
                'template' => 'Sonata/ContactAdmin/list_custom_user.html.twig'
            ]
        );
        $listMapper->add('email','string',
            [
                'template' => 'Sonata/ContactAdmin/list_custom_email.html.twig',
            ]
        );
        $listMapper->add('accepted');
    }

    public function createQuery($context = 'list')
    {
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $query = parent::createQuery($context);

        $query->andWhere(
            $query->expr()->eq($query->getRootAliases()[0] . '.fromUser', ':fromUser')
        );
        $query->setParameter('fromUser', $user);
        $query->orWhere(
            $query->expr()->eq($query->getRootAliases()[0] . '.toUser', ':toUser')
        );
        $query->setParameter('toUser', $user);

        return $query;
    }



    public function toString($object)
    {
        return $object instanceof Invitation
            ? $object->getEmail()
            : 'User ';
    }


    public function generateRandomString($length = 16, $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
    {
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}