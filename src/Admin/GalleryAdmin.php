<?php
/**
 * Created by fedandco.
 * User: tom79
 * Date: 09/03/19
 * Time: 17:05
 */

namespace App\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\Form\Type\CollectionType;
use Sonata\MediaBundle\Provider\Pool;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class GalleryAdmin extends AbstractAdmin
{

    /**
     * @var Pool
     */
    protected $pool;

    protected $classnameLabel = 'Gallery';

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     * @param Pool   $pool
     */
    public function __construct($code, $class, $baseControllerName, Pool $pool)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->pool = $pool;
    }

    /**
     * {@inheritdoc}
     */
    public function prePersist($gallery)
    {
        $parameters = $this->getPersistentParameters();
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $gallery->setOwner($user);
        $gallery->setContext($parameters['context']);
    }

    /**
     * {@inheritdoc}
     */
    public function postUpdate($gallery)
    {
        $gallery->reorderGalleryHasMedia();
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        if( $this->isCurrentRoute('edit') &&  !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('edit', $this->getSubject()))
            throw new AccessDeniedHttpException();
        // define group zoning
        $formMapper
            ->with('Gallery', ['class' => 'col-md-9'])->end()
            ->with('Options', ['class' => 'col-md-3'])->end()
        ;

        $context = $this->getPersistentParameter('context');

        if (!$context) {
            $context = $this->pool->getDefaultContext();
        }

        $formats = [];
        foreach ((array) $this->pool->getFormatNamesByContext($context) as $name => $options) {
            $formats[$name] = $name;
        }

        $contexts = [];
        foreach ((array) $this->pool->getContexts() as $contextItem => $format) {
            $contexts[$contextItem] = $contextItem;
        }

        $formMapper
            ->with('Options')
            ->add('context', ChoiceType::class, ['choices' => $contexts])
            ->add('enabled', null, ['required' => false])
            ->add('name')
            ->ifTrue($formats)
            ->add('defaultFormat', ChoiceType::class, ['choices' => $formats])
            ->ifEnd()
            ->end()
            ->with('Gallery')
            ->add('galleryHasMedias', CollectionType::class, ['by_reference' => false], [
                'edit' => 'inline',
                'inline' => 'table',
                'sortable' => 'position',
                'link_parameters' => ['context' => $context],
                'admin_code' => 'sonata.media.admin.gallery_has_media',
            ])
            ->end()
        ;
    }


    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('enabled', 'boolean', ['editable' => true])
            ->add('context', 'trans', ['catalogue' => 'SonataMediaBundle'])
            ->add('defaultFormat', 'trans', ['catalogue' => 'SonataMediaBundle'])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getPersistentParameters()
    {
        $parameters = parent::getPersistentParameters();

        if (!$this->hasRequest()) {
            return $parameters;
        }

        return array_merge($parameters, [
            'context' => $this->getRequest()->get('context', $this->pool->getDefaultContext()),
        ]);
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('delete');
        $collection->remove('list');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('enabled')
            ->add('context', null, [
                'show_filter' => false,
            ])
        ;
    }
    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper): void
    {
        if( !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('edit', $this->getSubject()))
            throw new AccessDeniedHttpException();

    }
}