<?php
/**
 * Created by fedandco.
 * User: tom79
 * Date: 06/03/19
 * Time: 11:43
 */

namespace App\Controller;


use App\Entity\Client;
use App\Entity\CustomField;
use App\Entity\MastodonAccount;
use App\Form\MastodonAccountType;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class MastodonAccountCRUDController extends CRUDController
{
    public function myCustomAction()
    {
        // your code here ...
    }

    public function editAction($id = null)
    {
        if( empty($id))
            throw new NotFoundHttpException();

        $AccountInDB = $this->getDoctrine()->getRepository("App:MastodonAccount")->findOneBy(
            [
                'id' => $id,
            ]
        );
        if( empty($AccountInDB))
            throw new NotFoundHttpException();
        if(!$this->get('security.authorization_checker')->isGranted('edit', $AccountInDB))
            throw new AccessDeniedHttpException();


        $fields = $AccountInDB->getFields();
        $numberOfMissingField = 4 - count($fields);
        for($i = 0 ; $i < $numberOfMissingField ; $i++){
            $AccountInDB->addField(new CustomField());
        }

        $form = $this->createForm(MastodonAccountType::class, $AccountInDB);
        $form->handleRequest($this->getRequest());
        if ($form->isSubmitted() && $form->isValid() && $AccountInDB instanceof MastodonAccount) {
            $params['display_name'] = $AccountInDB->getDisplayName();
            $params['note'] = $AccountInDB->getNote();
            $params['locked'] = $AccountInDB->getLocked();
            $i = 0;
            foreach ($AccountInDB->getFields() as $field){
                if( !empty($field->getName()) && !empty($field->getValue())){
                    $params['fields_attributes'][$i]['name'] = $field->getName();
                    $params['fields_attributes'][$i]['value'] = $field->getValue();
                }
                $i++;
            }
            $mastodonAPI = $this->get('mastodon.api');
            $mastodonAPI->set_token($AccountInDB->getToken(), "Bearer");
            $mastodonAPI->set_url("https://".$AccountInDB->getInstance());

            if( $AccountInDB->getAvatar()){
                $file = $form->get('avatar')->getData();
                $fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();
                try {
                    $file->move(
                        $this->getParameter('temp_upload'),
                        $fileName
                    );
                } catch (FileException $e) {}
                $paramImage['media']['path'] = $this->getParameter('temp_upload') . $fileName;
                $paramImage['media']['mimetype'] = $file->getClientMimeType();
                $paramImage['media']['filename'] = $fileName;
                $paramImage['media']['name'] = "avatar";
                $mastodonAPI->accounts_update_credentials($paramImage);
                @unlink($paramImage['media']['path']);
            }
            if( $AccountInDB->getHeader()){
                $file = $form->get('header')->getData();
                $fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();
                try {
                    $file->move(
                        $this->getParameter('temp_upload'),
                        $fileName
                    );
                } catch (FileException $e) {}
                $paramImage['media']['path'] = $this->getParameter('temp_upload') . $fileName;
                $paramImage['media']['mimetype'] = $file->getClientMimeType();
                $paramImage['media']['filename'] = $fileName;
                $paramImage['media']['name'] = "header";
                $mastodonAPI->accounts_update_credentials($paramImage);
                @unlink($paramImage['media']['path']);
            }
            $updateAccount = $mastodonAPI->accounts_update_credentials($params);
            if( isset($updateAccount['error'])){
                if( $updateAccount['error_message'])
                    $form->addError(new FormError( $updateAccount['error_message']));
                else
                    $form->addError(new FormError($this->trans('error.general',[],'fedandco','en','fe')));
            }else{
                $AccountInDB = $mastodonAPI->updateAccount($AccountInDB, $updateAccount['response']);
                $this->getDoctrine()->getManager()->persist($AccountInDB);
                $this->getDoctrine()->getManager()->flush();
            }
        }

        return $this->render('Client/profile_edit.html.twig', [
            'form' => $form->createView(),
            'action' => 'edit',
            'object' => $AccountInDB
        ]);
    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }

    public function createAction()
    {

        $client = new Client();
        $flow = $this->get('fedanco.form.flow.connectmastodon');

        $flow->bind($client);
        $form = $flow->createForm();
        $urlToMastodon = null;
        $client_id = null;
        $client_secret = null;
        if ($flow->isValid($form)) {
            $MastodonApi = $this->get('mastodon.api');
            if( $flow->getCurrentStep() == 1){
                $host = $client->getHost();
                $result = $this->get('mastodon.api')->getInstanceNodeInfo($host);
                //We currently only support Mastodon accounts
                if( $result != "MASTODON"  && $result != "PLEROMA"){
                    $form->get('host')->addError(new FormError($this->trans('error.instance.mastodon_only',[],'fedandco','en','fe')));
                }else{
                    $MastodonApi->set_url("https://" . $host);
                    $MastodonApi->set_scopes([]);
                    $createApp = $this->get('mastodon.api')->create_app("FedAndCo", [], '', "https://fedandco.fedilab.app");
                    if( isset($createApp['error']) ){
                        $form->get('host')->addError(new FormError($this->trans('error.instance.mastodon_client_id',[],'fedandco','en','fe')));
                    }else{
                        // form for the next step
                        $MastodonApi->set_client($createApp['response']['client_id'], $createApp['response']['client_secret']);
                        $urlToMastodon = $MastodonApi->getAuthorizationUrl();
                        if( isset($createApp['error']) ){
                            $form->get('host')->addError(new FormError($this->trans('error.instance.mastodon_oauth_url',[],'fedandco','en','fe')));
                        }else{
                            $flow->saveCurrentStepData($form);
                            $client_id = $createApp['response']['client_id'];
                            $client_secret = $createApp['response']['client_secret'];
                            $flow->nextStep();
                            $form = $flow->createForm();
                        }
                    }

                }

            } else if ($flow->getCurrentStep() == 2) {

                $code = $client->getCode();
                $MastodonApi->set_url("https://" . $client->getHost());
                $MastodonApi->set_scopes([]);

                $MastodonApi->set_client($client->getClientId(), $client->getClientSecret());
                $reply = $MastodonApi->loginAuthorization($code);
                if( isset($reply['error']) ){
                    $form->get('code')->addError(new FormError($this->trans('error.instance.mastodon_token',[],'fedandco','en','fe')));
                }else{
                    $access_token = $reply['response']['access_token'];
                    $token_type = $reply['response']['token_type'];
                    $MastodonApi->set_url("https://" . $client->getHost());
                    $MastodonApi->set_token($access_token, $token_type);

                    $accountReply = $MastodonApi->accounts_verify_credentials();

                    if( isset($accountReply['error']) ){
                        $form->get('code')->addError(new FormError($this->trans('error.instance.mastodon_account',[],'fedandco','en','fe')));
                    }else{

                        $AccountInDB = $this->getDoctrine()->getRepository("App:MastodonAccount")->findOneBy(
                            [
                                'account_id' => $accountReply['response']['id'],
                                'instance' => $client->getHost()
                            ]
                        );

                        if( $AccountInDB && $AccountInDB instanceof MastodonAccount){
                            $AccountInDB = $MastodonApi->updateAccount($AccountInDB, $accountReply['response']);
                            $clientInDB = $this->getDoctrine()->getRepository("App:Client")->findOneBy(
                                [
                                    'account' => $AccountInDB
                                ]
                            );
                            if( $clientInDB && $clientInDB instanceof Client){
                                $clientInDB->setClientId($client->getClientId());
                                $clientInDB->setClientSecret($client->getClientSecret());
                                $clientInDB->setCode($client->getCode());
                            }
                        }else{
                            $owner = $this->getUser();
                            $AccountInDB =  $MastodonApi->getSingleAccount($accountReply['response']);
                            $AccountInDB->setInstance($client->getHost());
                            $AccountInDB->setOwner($owner);
                            $clientInDB = $client;
                            $clientInDB->setAccount($AccountInDB);
                        }

                        if( $this->getUser() == $AccountInDB->getOwner()) {
                            $AccountInDB->setToken($access_token);

                            $this->getDoctrine()->getManager()->persist($AccountInDB);
                            $this->getDoctrine()->getManager()->persist($clientInDB);
                            $this->getDoctrine()->getManager()->flush();
                            return $this->redirectToRoute('admin_mastodon_list');
                        }else{
                            $form->get('code')->addError(new FormError($this->trans('error.instance.mastodon_account_already_used',[],'fedandco','en','fe')));
                        }
                    }
                }
            }

        }

        return $this->render('Client/connect.html.twig', [
            'form' => $form->createView(),
            'flow' => $flow,
            'urlToMastodon' => $urlToMastodon,
            'client_id' => $client_id,
            'client_secret' => $client_secret,
        ]);
    }
}