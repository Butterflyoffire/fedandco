<?php
/**
 * Created by fedandco.
 * User: tom79
 * Date: 14/04/19
 * Time: 19:16
 */

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{

    /**
     * @Route("/", name="default")
     */
    public function index()
    {
        return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
    }

    /**
     * @Route("/login", name="default")
     */
    public function login()
    {
        return $this->redirect($this->generateUrl('sonata_user_admin_security_login'));
    }
}