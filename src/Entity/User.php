<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 20/02/2019
 * Time: 10:26
 */

namespace App\Entity;
use App\Application\Sonata\UserBundle\Entity\Group;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Sonata\UserBundle\Entity\BaseUser as BaseUser;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 */
class User  extends BaseUser
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Project", mappedBy="contributors")
     */
    private $projects;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Project", mappedBy="owner", orphanRemoval=true)
     */
    private $OwnerProjects;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProjectRole", mappedBy="User", orphanRemoval=true)
     */
    private $projectRoles;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Invitation", mappedBy="fromUser", orphanRemoval=true)
     */
    private $invitations_sent;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Invitation", mappedBy="toUser", orphanRemoval=true)
     */
    private $invitations_received;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $twitter;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $facebook;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mastodon;


    /**
     * @var \App\Entity\Media
     * @ORM\ManyToOne(targetEntity="App\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */
    public $profile_picture;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MastodonAccount", mappedBy="account_owner", orphanRemoval=true)
     */
    private $mastodonAccounts;



	public function __construct()
     {
         parent::__construct();
         $this->projects = new ArrayCollection();
         $this->OwnerProjects = new ArrayCollection();
         $this->projectRoles = new ArrayCollection();
         $this->relations = new ArrayCollection();
         $this->invitations_sent = new ArrayCollection();
         $this->invitations_received = new ArrayCollection();
         $this->groups = new ArrayCollection();
         $this->galleries = new ArrayCollection();
         $this->mastodonAccounts = new ArrayCollection();
         // your own logic
     }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Project[]
     */
    public function getProjects(): Collection
    {
        return $this->projects;
    }

    public function addProject(Project $project): self
    {
        if (!$this->projects->contains($project)) {
            $this->projects[] = $project;
            $project->addContributor($this);
        }

        return $this;
    }

    public function removeProject(Project $project): self
    {
        if ($this->projects->contains($project)) {
            $this->projects->removeElement($project);
            $project->removeContributor($this);
        }

        return $this;
    }

    /**
     * @return Collection|Project[]
     */
    public function getOwnerProjects(): Collection
    {
        return $this->OwnerProjects;
    }

    public function addOwnerProject(Project $ownerProject): self
    {
        if (!$this->OwnerProjects->contains($ownerProject)) {
            $this->OwnerProjects[] = $ownerProject;
            $ownerProject->setOwner($this);
        }

        return $this;
    }

    public function removeOwnerProject(Project $ownerProject): self
    {
        if ($this->OwnerProjects->contains($ownerProject)) {
            $this->OwnerProjects->removeElement($ownerProject);
            // set the owning side to null (unless already changed)
            if ($ownerProject->getOwner() === $this) {
                $ownerProject->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProjectRole[]
     */
    public function getProjectRoles(): Collection
    {
        return $this->projectRoles;
    }

    public function addProjectRole(ProjectRole $projectRole): self
    {
        if (!$this->projectRoles->contains($projectRole)) {
            $this->projectRoles[] = $projectRole;
            $projectRole->setUser($this);
        }

        return $this;
    }

    public function removeProjectRole(ProjectRole $projectRole): self
    {
        if ($this->projectRoles->contains($projectRole)) {
            $this->projectRoles->removeElement($projectRole);
            // set the owning side to null (unless already changed)
            if ($projectRole->getUser() === $this) {
                $projectRole->setUser(null);
            }
        }

        return $this;
    }



    /**
     * @return Collection|Invitation[]
     */
    public function getInvitationsSent(): Collection
    {
        return $this->invitations_sent;
    }

    public function addInvitationsSent(Invitation $invitationsSent): self
    {
        if (!$this->invitations_sent->contains($invitationsSent)) {
            $this->invitations_sent[] = $invitationsSent;
            $invitationsSent->setFromUser($this);
        }

        return $this;
    }

    public function removeInvitationsSent(Invitation $invitationsSent): self
    {
        if ($this->invitations_sent->contains($invitationsSent)) {
            $this->invitations_sent->removeElement($invitationsSent);
            // set the owning side to null (unless already changed)
            if ($invitationsSent->getFromUser() === $this) {
                $invitationsSent->setFromUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Invitation[]
     */
    public function getInvitationsReceived(): Collection
    {
        return $this->invitations_received;
    }

    public function addInvitationsReceived(Invitation $invitationsReceived): self
    {
        if (!$this->invitations_received->contains($invitationsReceived)) {
            $this->invitations_received[] = $invitationsReceived;
            $invitationsReceived->setToUser($this);
        }

        return $this;
    }

    public function removeInvitationsReceived(Invitation $invitationsReceived): self
    {
        if ($this->invitations_received->contains($invitationsReceived)) {
            $this->invitations_received->removeElement($invitationsReceived);
            // set the owning side to null (unless already changed)
            if ($invitationsReceived->getToUser() === $this) {
                $invitationsReceived->setToUser(null);
            }
        }

        return $this;
    }

    public function getTwitter(): ?string
    {
        return $this->twitter;
    }

    public function setTwitter(?string $twitter): self
    {
        $this->twitter = $twitter;

        return $this;
    }

    public function getFacebook(): ?string
    {
        return $this->facebook;
    }

    public function setFacebook(?string $facebook): self
    {
        $this->facebook = $facebook;

        return $this;
    }

    public function getMastodon(): ?string
    {
        return $this->mastodon;
    }

    public function setMastodon(?string $mastodon): self
    {
        $this->mastodon = $mastodon;

        return $this;
    }

    public function getProfilePicture(): ?Media
    {
        return $this->profile_picture;
    }

    public function setProfilePicture(?Media $profile_picture): self
    {
        $this->profile_picture = $profile_picture;

        return $this;
    }

    /**
     * @return Collection|Group[]
     */
    public function getGroups(): Collection
    {
        return $this->groups;
    }

    /**
     * @return Collection|MastodonAccount[]
     */
    public function getMastodonAccounts(): Collection
    {
        return $this->mastodonAccounts;
    }

    public function addMastodonAccount(MastodonAccount $mastodonAccount): self
    {
        if (!$this->mastodonAccounts->contains($mastodonAccount)) {
            $this->mastodonAccounts[] = $mastodonAccount;
            $mastodonAccount->setOwner($this);
        }

        return $this;
    }

    public function removeMastodonAccount(MastodonAccount $mastodonAccount): self
    {
        if ($this->mastodonAccounts->contains($mastodonAccount)) {
            $this->mastodonAccounts->removeElement($mastodonAccount);
            // set the owning side to null (unless already changed)
            if ($mastodonAccount->getOwner() === $this) {
                $mastodonAccount->setOwner(null);
            }
        }

        return $this;
    }


    

}