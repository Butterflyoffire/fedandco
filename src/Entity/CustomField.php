<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CustomFieldRepository")
 */
class CustomField
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $value;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $verified_at;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MastodonAccount", inversedBy="Fields")
     */
    private $mastodonAccount;



    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getVerifiedAt(): ?\DateTimeInterface
    {
        return $this->verified_at;
    }

    public function setVerifiedAt(?\DateTimeInterface $verified_at): self
    {
        $this->verified_at = $verified_at;

        return $this;
    }

    public function getMastodonAccount(): ?MastodonAccount
    {
        return $this->mastodonAccount;
    }

    public function setMastodonAccount(?MastodonAccount $mastodonAccount): self
    {
        $this->mastodonAccount = $mastodonAccount;

        return $this;
    }

    public function __toString()
    {
        return $this->getName()?$this->getName():"";
    }
    
}
