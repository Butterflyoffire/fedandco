<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 */
class Project
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="OwnerProjects")
     * @ORM\JoinColumn(nullable=false)
     */
    private $owner;


    /**
     * @ORM\OneToOne(targetEntity="App\Entity\MastodonAccount", cascade={"persist"})
     */
    private $social_account;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProjectRole", mappedBy="project", orphanRemoval=true, cascade={"persist","remove"})
     */
    private $contributor_role;





    public function __construct()
    {
        $this->contributor_role = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }


    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }


    public function getSocialAccount(): ?MastodonAccount
    {
        return $this->social_account;
    }

    public function setSocialAccount(?MastodonAccount $social_account): self
    {
        $this->social_account = $social_account;

        return $this;
    }




    public function __toString()
    {
        return $this->getName()?$this->getName():"";
    }

    /**
     * @return Collection|ProjectRole[]
     */
    public function getContributorRole(): Collection
    {
        return $this->contributor_role;
    }

    public function addContributorRole(ProjectRole $projectRole): self
    {
        if (!$this->contributor_role->contains($projectRole)) {
            $this->contributor_role[] = $projectRole;
            $projectRole->setProject($this);
        }

        return $this;
    }

    public function removeContributorRole(ProjectRole $projectRole): self
    {
        if ($this->contributor_role->contains($projectRole)) {
            $this->contributor_role->removeElement($projectRole);
            // set the owning side to null (unless already changed)
            if ($projectRole->getProject() === $this) {
                $projectRole->setProject(null);
            }
        }

        return $this;
    }

    public function getGallery(): ?Gallery
    {
        return $this->gallery;
    }

    public function setGallery(?Gallery $gallery): self
    {
        $this->gallery = $gallery;

        return $this;
    }



}
