<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MessageRepository")
 */
class Message
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $content_warning;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $visibility;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Media", inversedBy="messages" ,cascade={"persist"})
     */
    private $attachments;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $scheduled_at;


    /**
     * @ORM\Column(type="boolean")
     */
    private $scheduled;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $sent_at;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MastodonAccount", inversedBy="messages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $social_account;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $in_reply_to_id;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isSensitive;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LogSent", mappedBy="message",cascade={"remove"})
     */
    private $logSents;




    public function getTotalMedia(){
        return count($this->getAttachments());
    }


    public function getSent(){
        return ($this->sent_at != null);
    }

    public function __construct()
    {
        $this->attachments = new ArrayCollection();
        $this->logSents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContentWarning(): ?string
    {
        return $this->content_warning;
    }

    public function setContentWarning(?string $content_warning): self
    {
        $this->content_warning = $content_warning;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getVisibility(): ?string
    {
        return $this->visibility;
    }

    public function setVisibility(string $visibility): self
    {
        $this->visibility = $visibility;

        return $this;
    }

    /**
     * @return Collection|Media[]
     */
    public function getAttachments(): Collection
    {
        return $this->attachments;
    }

    public function addAttachment(Media $attachment): self
    {
        if (!$this->attachments->contains($attachment)) {
            $this->attachments[] = $attachment;
        }

        return $this;
    }

    public function removeAttachment(Media $attachment): self
    {
        if ($this->attachments->contains($attachment)) {
            $this->attachments->removeElement($attachment);
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getScheduledAt(): ?\DateTimeInterface
    {
        return $this->scheduled_at;
    }

    public function setScheduledAt(?\DateTimeInterface $scheduled_at): self
    {
        $this->scheduled_at = $scheduled_at;

        return $this;
    }
    

    public function getScheduled(): ?bool
    {
        return $this->scheduled;
    }

    public function setScheduled(bool $scheduled): self
    {
        $this->scheduled = $scheduled;

        return $this;
    }

    public function getSentAt(): ?\DateTimeInterface
    {
        return $this->sent_at;
    }

    public function setSentAt(?\DateTimeInterface $sent_at): self
    {
        $this->sent_at = $sent_at;

        return $this;
    }

    /**
     * @return Collection|MastodonAccount[]
     */
    public function getAccount(): Collection
    {
        return $this->social_account;
    }

    public function addAccount(MastodonAccount $social_account): self
    {
        if (!$this->social_account->contains($social_account)) {
            $this->social_account[] = $social_account;
            $social_account->setMessage($this);
        }

        return $this;
    }

    public function removeAccount(MastodonAccount $social_account): self
    {
        if ($this->social_account->contains($social_account)) {
            $this->social_account->removeElement($social_account);
            // set the owning side to null (unless already changed)
            if ($social_account->getMessage() === $this) {
                $social_account->setMessage(null);
            }
        }

        return $this;
    }

    public function getSocialAccount(): ?MastodonAccount
    {
        return $this->social_account;
    }

    public function setSocialAccount(?MastodonAccount $social_account): self
    {
        $this->social_account = $social_account;

        return $this;
    }

    public function getInReplyToId(): ?string
    {
        return $this->in_reply_to_id;
    }

    public function setInReplyToId(?string $in_reply_to_id): self
    {
        $this->in_reply_to_id = $in_reply_to_id;

        return $this;
    }

    public function getIsSensitive(): ?bool
    {
        return $this->isSensitive;
    }

    public function setIsSensitive(?bool $isSensitive): self
    {
        $this->isSensitive = $isSensitive;

        return $this;
    }

    /**
     * @return Collection|LogSent[]
     */
    public function getLogSents(): Collection
    {
        return $this->logSents;
    }

    public function addLogSent(LogSent $logSent): self
    {
        if (!$this->logSents->contains($logSent)) {
            $this->logSents[] = $logSent;
            $logSent->setMessage($this);
        }

        return $this;
    }

    public function removeLogSent(LogSent $logSent): self
    {
        if ($this->logSents->contains($logSent)) {
            $this->logSents->removeElement($logSent);
            // set the owning side to null (unless already changed)
            if ($logSent->getMessage() === $this) {
                $logSent->setMessage(null);
            }
        }

        return $this;
    }



}
