<?php

namespace App\Repository;

use App\Entity\Project;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\Query\Expr;

/**
 * @method Project|null find($id, $lockMode = null, $lockVersion = null)
 * @method Project|null findOneBy(array $criteria, array $orderBy = null)
 * @method Project[]    findAll()
 * @method Project[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjectRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Project::class);
    }



    public function viewableAccounts($user)
    {
        return $this->createQueryBuilder('p')
            ->leftJoin("p.social_account", "a", Expr\Join::WITH, 'p.social_account = a.id')
            ->leftJoin("p.contributor_role", "pr")
            ->where(
                'p.social_account IS NOT NULL AND (p.owner = :user OR pr.User = :user OR a.account_owner = :user)'
            )
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function canEdit($project, $user):?int
    {
        return $this->createQueryBuilder('p')
            ->select('count(p.id)')
            ->leftJoin('App:ProjectRole', 'pr', Expr\Join::WITH, 'p.id = pr.project')
            ->where(
                'p.id = :project_id AND (p.owner = :user OR (pr.User = :user AND pr.role = \'ADMIN\'))'
            )
            ->setParameter('user', $user)
            ->setParameter(":project_id", $project)
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

    /**
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function canView($project, $user):?int
    {
        return $this->createQueryBuilder('p')
            ->select('count(p.id)')
            ->leftJoin('App:ProjectRole', 'pr', Expr\Join::WITH, 'p.id = pr.project')
            ->where(
                'p.id = :project_id AND (p.owner = :user OR (pr.User = :user ))'
            )
            ->setParameter('user', $user)
            ->setParameter(":project_id", $project)
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }

    // /**
    //  * @return Project[] Returns an array of Project objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Project
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
