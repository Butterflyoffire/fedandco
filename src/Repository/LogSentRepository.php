<?php

namespace App\Repository;

use App\Entity\LogSent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method LogSent|null find($id, $lockMode = null, $lockVersion = null)
 * @method LogSent|null findOneBy(array $criteria, array $orderBy = null)
 * @method LogSent[]    findAll()
 * @method LogSent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LogSentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, LogSent::class);
    }

    // /**
    //  * @return LogSent[] Returns an array of LogSent objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LogSent
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
