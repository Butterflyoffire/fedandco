<?php

namespace App\Repository;

use App\Entity\Message;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\Query\Expr;

/**
 * @method Message|null find($id, $lockMode = null, $lockVersion = null)
 * @method Message|null findOneBy(array $criteria, array $orderBy = null)
 * @method Message[]    findAll()
 * @method Message[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Message::class);
    }

    /**
     * @param $message
     * @param $user
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function canEdit($message, $user):?int
    {
        return $this->createQueryBuilder('m')
            ->select('count(m.id)')
            ->leftJoin('App:MastodonAccount', 'a', Expr\Join::WITH, 'm.social_account = a.id')
            ->leftJoin('App:Project', 'p', Expr\Join::WITH, 'm.social_account = p.social_account')
            ->leftJoin('App:ProjectRole', 'pr', Expr\Join::WITH, 'p.id = pr.project')
            ->where(
                'm.id = :message AND (a.account_owner = :user OR p.owner = :user OR (pr.User = :user AND (pr.role = \'ADMIN\' OR pr.role = \'AUTHOR\' )))'
            )
            ->setParameter('message', $message)
            ->setParameter('user', $user)
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }

    // /**
    //  * @return Message[] Returns an array of Message objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Message
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
