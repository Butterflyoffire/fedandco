<?php
/**
 * Created by fedandco.
 * User: tom79
 * Date: 12/04/19
 * Time: 18:33
 */

namespace App\Command;


use App\Entity\LogSent;
use App\Entity\Message;
use App\Services\Mastodon_api;
use Doctrine\ORM\EntityManagerInterface;
use Sonata\MediaBundle\Provider\FileProvider;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SendMessageCommand extends Command
{

    protected static $defaultName = 'fedandco:send:status';
    private $em;
    private $ma;
    private $fp;

    public function __construct(EntityManagerInterface $em, Mastodon_api $mastodon_api, FileProvider $fileProvider){
        $this->em = $em;
        $this->ma = $mastodon_api;
        $this->fp = $fileProvider;
        parent::__construct();
    }

    protected function configure(){
        $this
            ->setDescription('Send a status')
            ->setHelp('This command allows you to send a status that was previously stored in db')
            ->addArgument('message_id', InputArgument::REQUIRED, 'ID of the status');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \ErrorException
     */
    protected function execute(InputInterface $input, OutputInterface $output){
        $message_id =  $input->getArgument('message_id');
        $message = $this->em->getRepository("App:Message")->findOneBy(['id' => $message_id]);
        /** @var $message Message **/
        if( !isset($message) || $message->getSent()){
            $output->writeln('Cannot send the message with id '. $message_id);
            return;
        }
        //Set the social account
        $account = $message->getSocialAccount();
        //Initialize the api with instance value and token value
        $this->ma->set_url("https://" . $account->getInstance());
        $this->ma->set_token($account->getToken(), "Bearer");

        //$params will contain all parameter values for submitting the message

        //First step is to upload media if available
        if( $message->getAttachments() && count($message->getAttachments()) > 0){
            foreach ($message->getAttachments() as $attachment){

                $paramImage['media']['path'] = __DIR__ . '/../../public/uploads/media/' .$this->fp->getReferenceFile($attachment)->getName();
                $paramImage['media']['mimetype'] = $attachment->getContentType();
                $paramImage['media']['filename'] = str_replace($this->fp->generatePath($attachment)."/", "",$this->fp->getReferenceFile($attachment)->getName());
                $paramImage['media']['name'] = "file";
                $paramImage['description'] = $attachment->getDescription();
                $reply = $this->ma->post_media($paramImage);
                if( isset($reply['error'])){
                    $error_message = 'Cannot upload media '.$attachment->getName() . " with id: " . $attachment->getId();
                    try {
                        $logSent = new LogSent();
                        $logSent->setLoggedAt(new \DateTime());
                        $logSent->setMessage($message);
                        $logSent->setSent(false);
                        //More info about the error if available
                        if( $reply['error_message']) {
                            $error_message.= " : ".$reply['error_message'];
                        }
                        $logSent->setInfo($error_message);
                        $this->em->persist($logSent);
                        $this->em->flush();
                    } catch (\Exception $e) {}
                    throw new HttpException(500, $error_message);
                }else{
                    $_a = $this->ma->getSingleAttachment($reply['response']);
                    $params['media_ids'][] = $_a->getId();
                }
            }
        }


        //Add the reply id if it exists
        if( $message->getInReplyToId()){
            $params['in_reply_to_id	'] = $message->getInReplyToId();
        }
        //Add the spoiler text if it exists
        if( $message->getContentWarning()){
            $params['spoiler_text'] = $message->getContentWarning();
        }
        if( $message->getContent()) {
            $params['status'] = $message->getContent();
        }else{
            $params['status'] = "";
        }
        //Add sensitivity of the message
        $params['sensitive'] = ($message->getIsSensitive() == null || !$message->getIsSensitive())?false:true;
        //Add visibility
        $params['visibility'] = $message->getVisibility();
        $response = $this->ma->post_statuses($params);

        //Log some values
        $logSent = new LogSent();
        try {
            $logSent->setLoggedAt(new \DateTime());
            $logSent->setMessage($message);
            if( isset($response['error'])){
                $logSent->setSent(false);
                if( $response['error_message']) {
                    $logSent->setInfo($response['error_message']);
                }
            }else{
                $logSent->setSent(true);
                $message->setSentAt(new \DateTime());
                $this->em->persist($message);
                $this->em->flush();
            }
            $this->em->persist($logSent);
            $this->em->flush();
        } catch (\Exception $e) {}
    }
}